provider "aws" {
  region = "ap-southeast-2"
}

terraform {
  backend "s3" {
    bucket     = "gadevs-tfstate"
    key        = "test/terraform-ec2-userdata.tfstate"
    region     = "ap-southeast-2"
    lock_table = "terraform-lock"
  }
}

resource "aws_instance" "www" {
  ami           = var.ami_id
  instance_type = "t2.micro"
  user_data     = file("userdata.sh")

  tags = {
    owner = "your.email@ga.gov.au"
    Name  = "terraform-ec2-userdata"
  }
}
